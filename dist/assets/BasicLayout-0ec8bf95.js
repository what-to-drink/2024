import{d as D,r as y,o as N,a as C,c as A,b as _,_ as F,e as O,w as L,f as T,g as R,u as m,h as I,i as q,t as M,n as $,j as E,k as G,l as z,F as j,m as k}from"./index-2460668d.js";import{_ as P,u as V,g as H,a as Y,r as K}from"./visit-4f1d4d34.js";import{u as W}from"./useLongPress-5474228b.js";function X(d){return[(d>>16&255)/255,(d>>8&255)/255,(255&d)/255]}["SCREEN","LINEAR_LIGHT"].reduce((d,i,r)=>Object.assign(d,{[i]:r}),{});class J{constructor(i,r,v,h=!1){const a=this,p=document.location.search.toLowerCase().indexOf("debug=webgl")!==-1;a.canvas=i,a.gl=a.canvas.getContext("webgl",{antialias:!0}),a.meshes=[];const n=a.gl;r&&v&&this.setSize(r,v),a.lastDebugMsg,a.debug=h&&p?function(s){const l=new Date;l-a.lastDebugMsg>1e3&&console.log("---"),console.log(l.toLocaleTimeString()+Array(Math.max(0,32-s.length)).join(" ")+s+": ",...Array.from(arguments).slice(1)),a.lastDebugMsg=l}:()=>{},Object.defineProperties(a,{Material:{enumerable:!1,value:class{constructor(s,l,e={}){const t=this;function c(S,g){const f=n.createShader(S);return n.shaderSource(f,g),n.compileShader(f),n.getShaderParameter(f,n.COMPILE_STATUS)||console.error(n.getShaderInfoLog(f)),a.debug("Material.compileShaderSource",{source:g}),f}function u(S,g){return Object.entries(S).map(([f,x])=>x.getDeclaration(f,g)).join(`
`)}t.uniforms=e,t.uniformInstances=[];const b=`
              precision highp float;
            `;t.vertexSource=`
              ${b}
              attribute vec4 position;
              attribute vec2 uv;
              attribute vec2 uvNorm;
              ${u(a.commonUniforms,"vertex")}
              ${u(e,"vertex")}
              ${s}
            `,t.Source=`
              ${b}
              ${u(a.commonUniforms,"fragment")}
              ${u(e,"fragment")}
              ${l}
            `,t.vertexShader=c(n.VERTEX_SHADER,t.vertexSource),t.fragmentShader=c(n.FRAGMENT_SHADER,t.Source),t.program=n.createProgram(),n.attachShader(t.program,t.vertexShader),n.attachShader(t.program,t.fragmentShader),n.linkProgram(t.program),n.getProgramParameter(t.program,n.LINK_STATUS)||console.error(n.getProgramInfoLog(t.program)),n.useProgram(t.program),t.attachUniforms(void 0,a.commonUniforms),t.attachUniforms(void 0,t.uniforms)}attachUniforms(s,l){const e=this;s===void 0?Object.entries(l).forEach(([t,c])=>{e.attachUniforms(t,c)}):l.type=="array"?l.value.forEach((t,c)=>e.attachUniforms(`${s}[${c}]`,t)):l.type=="struct"?Object.entries(l.value).forEach(([t,c])=>e.attachUniforms(`${s}.${t}`,c)):(a.debug("Material.attachUniforms",{name:s,uniform:l}),e.uniformInstances.push({uniform:l,location:n.getUniformLocation(e.program,s)}))}}},Uniform:{enumerable:!1,value:class{constructor(s){this.type="float",Object.assign(this,s),this.typeFn={float:"1f",int:"1i",vec2:"2fv",vec3:"3fv",vec4:"4fv",mat4:"Matrix4fv"}[this.type]||"1f",this.update()}update(s){this.value!==void 0&&n[`uniform${this.typeFn}`](s,this.typeFn.indexOf("Matrix")===0?this.transpose:this.value,this.typeFn.indexOf("Matrix")===0?this.value:null)}getDeclaration(s,l,e){const t=this;if(t.excludeFrom!==l){if(t.type==="array")return t.value[0].getDeclaration(s,l,t.value.length)+`
const int ${s}_length = ${t.value.length};`;if(t.type==="struct"){let c=s.replace("u_","");return c=c.charAt(0).toUpperCase()+c.slice(1),`uniform struct ${c} 
                                {
`+Object.entries(t.value).map(([u,b])=>b.getDeclaration(u,l).replace(/^uniform/,"")).join("")+`
} ${s}${e>0?`[${e}]`:""};`}return`uniform ${t.type} ${s}${e>0?`[${e}]`:""};`}}}},PlaneGeometry:{enumerable:!1,value:class{constructor(s,l,e,t,c){n.createBuffer(),this.attributes={position:new a.Attribute({target:n.ARRAY_BUFFER,size:3}),uv:new a.Attribute({target:n.ARRAY_BUFFER,size:2}),uvNorm:new a.Attribute({target:n.ARRAY_BUFFER,size:2}),index:new a.Attribute({target:n.ELEMENT_ARRAY_BUFFER,size:3,type:n.UNSIGNED_SHORT})},this.setTopology(e,t),this.setSize(s,l,c)}setTopology(s=1,l=1){const e=this;e.xSegCount=s,e.ySegCount=l,e.vertexCount=(e.xSegCount+1)*(e.ySegCount+1),e.quadCount=e.xSegCount*e.ySegCount*2,e.attributes.uv.values=new Float32Array(2*e.vertexCount),e.attributes.uvNorm.values=new Float32Array(2*e.vertexCount),e.attributes.index.values=new Uint16Array(3*e.quadCount);for(let t=0;t<=e.ySegCount;t++)for(let c=0;c<=e.xSegCount;c++){const u=t*(e.xSegCount+1)+c;if(e.attributes.uv.values[2*u]=c/e.xSegCount,e.attributes.uv.values[2*u+1]=1-t/e.ySegCount,e.attributes.uvNorm.values[2*u]=c/e.xSegCount*2-1,e.attributes.uvNorm.values[2*u+1]=1-t/e.ySegCount*2,c<e.xSegCount&&t<e.ySegCount){const b=t*e.xSegCount+c;e.attributes.index.values[6*b]=u,e.attributes.index.values[6*b+1]=u+1+e.xSegCount,e.attributes.index.values[6*b+2]=u+1,e.attributes.index.values[6*b+3]=u+1,e.attributes.index.values[6*b+4]=u+1+e.xSegCount,e.attributes.index.values[6*b+5]=u+2+e.xSegCount}}e.attributes.uv.update(),e.attributes.uvNorm.update(),e.attributes.index.update(),a.debug("Geometry.setTopology",{uv:e.attributes.uv,uvNorm:e.attributes.uvNorm,index:e.attributes.index})}setSize(s=1,l=1,e="xz"){const t=this;t.width=s,t.height=l,t.orientation=e,t.attributes.position.values&&t.attributes.position.values.length===3*t.vertexCount||(t.attributes.position.values=new Float32Array(3*t.vertexCount));const c=s/-2,u=l/-2,b=s/t.xSegCount,S=l/t.ySegCount;for(let g=0;g<=t.ySegCount;g++){const f=u+g*S;for(let x=0;x<=t.xSegCount;x++){const B=c+x*b,U=g*(t.xSegCount+1)+x;t.attributes.position.values[3*U+"xyz".indexOf(e[0])]=B,t.attributes.position.values[3*U+"xyz".indexOf(e[1])]=-f}}t.attributes.position.update(),a.debug("Geometry.setSize",{position:t.attributes.position})}}},Mesh:{enumerable:!1,value:class{constructor(s,l){const e=this;e.geometry=s,e.material=l,e.wireframe=!1,e.attributeInstances=[],Object.entries(e.geometry.attributes).forEach(([t,c])=>{e.attributeInstances.push({attribute:c,location:c.attach(t,e.material.program)})}),a.meshes.push(e),a.debug("Mesh.constructor",{mesh:e})}draw(){n.useProgram(this.material.program),this.material.uniformInstances.forEach(({uniform:s,location:l})=>s.update(l)),this.attributeInstances.forEach(({attribute:s,location:l})=>s.use(l)),n.drawElements(this.wireframe?n.LINES:n.TRIANGLES,this.geometry.attributes.index.values.length,n.UNSIGNED_SHORT,0)}remove(){a.meshes=a.meshes.filter(s=>s!=this)}}},Attribute:{enumerable:!1,value:class{constructor(s){this.type=n.FLOAT,this.normalized=!1,this.buffer=n.createBuffer(),Object.assign(this,s),this.update()}update(){this.values!==void 0&&(n.bindBuffer(this.target,this.buffer),n.bufferData(this.target,this.values,n.STATIC_DRAW))}attach(s,l){const e=n.getAttribLocation(l,s);return this.target===n.ARRAY_BUFFER&&(n.enableVertexAttribArray(e),n.vertexAttribPointer(e,this.size,this.type,this.normalized,0,0)),e}use(s){n.bindBuffer(this.target,this.buffer),this.target===n.ARRAY_BUFFER&&(n.enableVertexAttribArray(s),n.vertexAttribPointer(s,this.size,this.type,this.normalized,0,0))}}}});const w=[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1];a.commonUniforms={projectionMatrix:new a.Uniform({type:"mat4",value:w}),modelViewMatrix:new a.Uniform({type:"mat4",value:w}),resolution:new a.Uniform({type:"vec2",value:[1,1]}),aspectRatio:new a.Uniform({type:"float",value:1})}}setSize(i=640,r=480){this.width=i,this.height=r,this.canvas.width=i,this.canvas.height=r,this.gl.viewport(0,0,i,r),this.commonUniforms.resolution.value=[i,r],this.commonUniforms.aspectRatio.value=i/r,this.debug("MiniGL.setSize",{width:i,height:r})}setOrthographicCamera(i=0,r=0,v=0,h=-2e3,a=2e3){this.commonUniforms.projectionMatrix.value=[2/this.width,0,0,0,0,2/this.height,0,0,0,0,2/(h-a),0,i,r,v,1],this.debug("setOrthographicCamera",this.commonUniforms.projectionMatrix.value)}render(){this.gl.clearColor(0,0,0,0),this.gl.clearDepth(1),this.meshes.forEach(i=>i.draw())}}function o(d,i,r){return i in d?Object.defineProperty(d,i,{value:r,enumerable:!0,configurable:!0,writable:!0}):d[i]=r,d}class Q{constructor(...i){o(this,"el",void 0),o(this,"cssVarRetries",0),o(this,"maxCssVarRetries",200),o(this,"angle",0),o(this,"isLoadedClass",!1),o(this,"isScrolling",!1),o(this,"scrollingTimeout",void 0),o(this,"scrollingRefreshDelay",200),o(this,"isIntersecting",!1),o(this,"shaderFiles",void 0),o(this,"vertexShader",void 0),o(this,"sectionColors",void 0),o(this,"computedCanvasStyle",void 0),o(this,"conf",void 0),o(this,"uniforms",void 0),o(this,"t",1253106),o(this,"last",0),o(this,"width",void 0),o(this,"minWidth",1111),o(this,"height",600),o(this,"xSegCount",void 0),o(this,"ySegCount",void 0),o(this,"mesh",void 0),o(this,"material",void 0),o(this,"geometry",void 0),o(this,"minigl",void 0),o(this,"scrollObserver",void 0),o(this,"amp",320),o(this,"seed",5),o(this,"freqX",14e-5),o(this,"freqY",29e-5),o(this,"freqDelta",1e-5),o(this,"activeColors",[1,1,1,1]),o(this,"isMetaKey",!1),o(this,"isGradientLegendVisible",!1),o(this,"isMouseDown",!1),o(this,"handleScroll",()=>{clearTimeout(this.scrollingTimeout),this.scrollingTimeout=setTimeout(this.handleScrollEnd,this.scrollingRefreshDelay),this.isGradientLegendVisible&&this.hideGradientLegend(),this.conf.playing&&(this.isScrolling=!0,this.pause())}),o(this,"handleScrollEnd",()=>{this.isScrolling=!1,this.isIntersecting&&this.play()}),o(this,"resize",()=>{this.width=window.innerWidth,this.minigl.setSize(this.width,this.height),this.minigl.setOrthographicCamera(),this.xSegCount=Math.ceil(this.width*this.conf.density[0]),this.ySegCount=Math.ceil(this.height*this.conf.density[1]),this.mesh.geometry.setTopology(this.xSegCount,this.ySegCount),this.mesh.geometry.setSize(this.width,this.height),this.mesh.material.uniforms.u_shadow_power.value=this.width<600?5:6}),o(this,"handleMouseDown",r=>{this.isGradientLegendVisible&&(this.isMetaKey=r.metaKey,this.isMouseDown=!0,this.conf.playing===!1&&requestAnimationFrame(this.animate))}),o(this,"handleMouseUp",()=>{this.isMouseDown=!1}),o(this,"animate",r=>{if(!this.shouldSkipFrame(r)||this.isMouseDown){if(this.t+=Math.min(r-this.last,1e3/15),this.last=r,this.isMouseDown){let v=160;this.isMetaKey&&(v=-160),this.t+=v}this.mesh.material.uniforms.u_time.value=this.t,this.minigl.render()}if(this.last!==0&&this.isStatic)return this.minigl.render(),void this.disconnect();(this.conf.playing||this.isMouseDown)&&requestAnimationFrame(this.animate)}),o(this,"addIsLoadedClass",()=>{!this.isLoadedClass&&(this.isLoadedClass=!0,this.el.classList.add("isLoaded"),setTimeout(()=>{this.el.parentElement.classList.add("isLoaded")},3e3))}),o(this,"pause",()=>{this.conf.playing=!1}),o(this,"play",()=>{requestAnimationFrame(this.animate),this.conf.playing=!0}),o(this,"initGradient",r=>(this.el=document.querySelector(r),this.connect(),this))}async connect(){this.shaderFiles={vertex:`varying vec3 v_color;

void main() {
  float time = u_time * u_global.noiseSpeed;

  vec2 noiseCoord = resolution * uvNorm * u_global.noiseFreq;

  vec2 st = 1. - uvNorm.xy;

  //
  // Tilting the plane
  //

  // Front-to-back tilt
  float tilt = resolution.y / 2.0 * uvNorm.y;

  // Left-to-right angle
  float incline = resolution.x * uvNorm.x / 2.0 * u_vertDeform.incline;

  // Up-down shift to offset incline
  float offset = resolution.x / 2.0 * u_vertDeform.incline * mix(u_vertDeform.offsetBottom, u_vertDeform.offsetTop, uv.y);

  //
  // Vertex noise
  //

  float noise = snoise(vec3(
    noiseCoord.x * u_vertDeform.noiseFreq.x + time * u_vertDeform.noiseFlow,
    noiseCoord.y * u_vertDeform.noiseFreq.y,
    time * u_vertDeform.noiseSpeed + u_vertDeform.noiseSeed
  )) * u_vertDeform.noiseAmp;

  // Fade noise to zero at edges
  noise *= 1.0 - pow(abs(uvNorm.y), 2.0);

  // Clamp to 0
  noise = max(0.0, noise);

  vec3 pos = vec3(
    position.x,
    position.y + tilt + incline + noise - offset,
    position.z
  );

  //
  // Vertex color, to be passed to fragment shader
  //

  if (u_active_colors[0] == 1.) {
    v_color = u_baseColor;
  }

  for (int i = 0; i < u_waveLayers_length; i++) {
    if (u_active_colors[i + 1] == 1.) {
      WaveLayers layer = u_waveLayers[i];

      float noise = smoothstep(
        layer.noiseFloor,
        layer.noiseCeil,
        snoise(vec3(
          noiseCoord.x * layer.noiseFreq.x + time * layer.noiseFlow,
          noiseCoord.y * layer.noiseFreq.y,
          time * layer.noiseSpeed + layer.noiseSeed
        )) / 2.0 + 0.5
      );

      v_color = blendNormal(v_color, layer.color, pow(noise, 4.));
    }
  }

  //
  // Finish
  //

  gl_Position = projectionMatrix * modelViewMatrix * vec4(pos, 1.0);
}`,noise:`//
// Description : Array and textureless GLSL 2D/3D/4D simplex
//               noise functions.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
//

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x) {
    return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

float snoise(vec3 v)
{
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

// First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

// Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //   x0 = x0 - 0.0 + 0.0 * C.xxx;
  //   x1 = x0 - i1  + 1.0 * C.xxx;
  //   x2 = x0 - i2  + 2.0 * C.xxx;
  //   x3 = x0 - 1.0 + 3.0 * C.xxx;
  vec3 x1 = x0 - i1 + C.xxx;
  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
  vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y

// Permutations
  i = mod289(i);
  vec4 p = permute( permute( permute(
            i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
          + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))
          + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

// Gradients: 7x7 points over a square, mapped onto an octahedron.
// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
  float n_ = 0.142857142857; // 1.0/7.0
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;
  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;
  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

//Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

// Mix final noise value
  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1),
                                dot(p2,x2), dot(p3,x3) ) );
}`,blend:`//
// https://github.com/jamieowen/glsl-blend
//

// Normal

vec3 blendNormal(vec3 base, vec3 blend) {
	return blend;
}

vec3 blendNormal(vec3 base, vec3 blend, float opacity) {
	return (blendNormal(base, blend) * opacity + base * (1.0 - opacity));
}

// Screen

float blendScreen(float base, float blend) {
	return 1.0-((1.0-base)*(1.0-blend));
}

vec3 blendScreen(vec3 base, vec3 blend) {
	return vec3(blendScreen(base.r,blend.r),blendScreen(base.g,blend.g),blendScreen(base.b,blend.b));
}

vec3 blendScreen(vec3 base, vec3 blend, float opacity) {
	return (blendScreen(base, blend) * opacity + base * (1.0 - opacity));
}

// Multiply

vec3 blendMultiply(vec3 base, vec3 blend) {
	return base*blend;
}

vec3 blendMultiply(vec3 base, vec3 blend, float opacity) {
	return (blendMultiply(base, blend) * opacity + base * (1.0 - opacity));
}

// Overlay

float blendOverlay(float base, float blend) {
	return base<0.5?(2.0*base*blend):(1.0-2.0*(1.0-base)*(1.0-blend));
}

vec3 blendOverlay(vec3 base, vec3 blend) {
	return vec3(blendOverlay(base.r,blend.r),blendOverlay(base.g,blend.g),blendOverlay(base.b,blend.b));
}

vec3 blendOverlay(vec3 base, vec3 blend, float opacity) {
	return (blendOverlay(base, blend) * opacity + base * (1.0 - opacity));
}

// Hard light

vec3 blendHardLight(vec3 base, vec3 blend) {
	return blendOverlay(blend,base);
}

vec3 blendHardLight(vec3 base, vec3 blend, float opacity) {
	return (blendHardLight(base, blend) * opacity + base * (1.0 - opacity));
}

// Soft light

float blendSoftLight(float base, float blend) {
	return (blend<0.5)?(2.0*base*blend+base*base*(1.0-2.0*blend)):(sqrt(base)*(2.0*blend-1.0)+2.0*base*(1.0-blend));
}

vec3 blendSoftLight(vec3 base, vec3 blend) {
	return vec3(blendSoftLight(base.r,blend.r),blendSoftLight(base.g,blend.g),blendSoftLight(base.b,blend.b));
}

vec3 blendSoftLight(vec3 base, vec3 blend, float opacity) {
	return (blendSoftLight(base, blend) * opacity + base * (1.0 - opacity));
}

// Color dodge

float blendColorDodge(float base, float blend) {
	return (blend==1.0)?blend:min(base/(1.0-blend),1.0);
}

vec3 blendColorDodge(vec3 base, vec3 blend) {
	return vec3(blendColorDodge(base.r,blend.r),blendColorDodge(base.g,blend.g),blendColorDodge(base.b,blend.b));
}

vec3 blendColorDodge(vec3 base, vec3 blend, float opacity) {
	return (blendColorDodge(base, blend) * opacity + base * (1.0 - opacity));
}

// Color burn

float blendColorBurn(float base, float blend) {
	return (blend==0.0)?blend:max((1.0-((1.0-base)/blend)),0.0);
}

vec3 blendColorBurn(vec3 base, vec3 blend) {
	return vec3(blendColorBurn(base.r,blend.r),blendColorBurn(base.g,blend.g),blendColorBurn(base.b,blend.b));
}

vec3 blendColorBurn(vec3 base, vec3 blend, float opacity) {
	return (blendColorBurn(base, blend) * opacity + base * (1.0 - opacity));
}

// Vivid Light

float blendVividLight(float base, float blend) {
	return (blend<0.5)?blendColorBurn(base,(2.0*blend)):blendColorDodge(base,(2.0*(blend-0.5)));
}

vec3 blendVividLight(vec3 base, vec3 blend) {
	return vec3(blendVividLight(base.r,blend.r),blendVividLight(base.g,blend.g),blendVividLight(base.b,blend.b));
}

vec3 blendVividLight(vec3 base, vec3 blend, float opacity) {
	return (blendVividLight(base, blend) * opacity + base * (1.0 - opacity));
}

// Lighten

float blendLighten(float base, float blend) {
	return max(blend,base);
}

vec3 blendLighten(vec3 base, vec3 blend) {
	return vec3(blendLighten(base.r,blend.r),blendLighten(base.g,blend.g),blendLighten(base.b,blend.b));
}

vec3 blendLighten(vec3 base, vec3 blend, float opacity) {
	return (blendLighten(base, blend) * opacity + base * (1.0 - opacity));
}

// Linear burn

float blendLinearBurn(float base, float blend) {
	// Note : Same implementation as BlendSubtractf
	return max(base+blend-1.0,0.0);
}

vec3 blendLinearBurn(vec3 base, vec3 blend) {
	// Note : Same implementation as BlendSubtract
	return max(base+blend-vec3(1.0),vec3(0.0));
}

vec3 blendLinearBurn(vec3 base, vec3 blend, float opacity) {
	return (blendLinearBurn(base, blend) * opacity + base * (1.0 - opacity));
}

// Linear dodge

float blendLinearDodge(float base, float blend) {
	// Note : Same implementation as BlendAddf
	return min(base+blend,1.0);
}

vec3 blendLinearDodge(vec3 base, vec3 blend) {
	// Note : Same implementation as BlendAdd
	return min(base+blend,vec3(1.0));
}

vec3 blendLinearDodge(vec3 base, vec3 blend, float opacity) {
	return (blendLinearDodge(base, blend) * opacity + base * (1.0 - opacity));
}

// Linear light

float blendLinearLight(float base, float blend) {
	return blend<0.5?blendLinearBurn(base,(2.0*blend)):blendLinearDodge(base,(2.0*(blend-0.5)));
}

vec3 blendLinearLight(vec3 base, vec3 blend) {
	return vec3(blendLinearLight(base.r,blend.r),blendLinearLight(base.g,blend.g),blendLinearLight(base.b,blend.b));
}

vec3 blendLinearLight(vec3 base, vec3 blend, float opacity) {
	return (blendLinearLight(base, blend) * opacity + base * (1.0 - opacity));
}`,fragment:`varying vec3 v_color;

void main() {
  vec3 color = v_color;
  if (u_darken_top == 1.0) {
    vec2 st = gl_FragCoord.xy/resolution.xy;
    color.g -= pow(st.y + sin(-12.0) * st.x, u_shadow_power) * 0.4;
  }
  gl_FragColor = vec4(color, 1.0);
}`},this.conf={presetName:"",wireframe:!1,density:[.06,.16],zoom:1,rotation:0,playing:!0},document.querySelectorAll("canvas").length<1?console.log("DID NOT LOAD HERO STRIPE CANVAS"):(this.minigl=new J(this.el,null,null,!0),requestAnimationFrame(()=>{this.el&&(this.computedCanvasStyle=getComputedStyle(this.el),this.waitForCssVars())}))}disconnect(){this.scrollObserver&&(window.removeEventListener("scroll",this.handleScroll),window.removeEventListener("mousedown",this.handleMouseDown),window.removeEventListener("mouseup",this.handleMouseUp),window.removeEventListener("keydown",this.handleKeyDown),this.scrollObserver.disconnect()),window.removeEventListener("resize",this.resize)}initMaterial(){this.uniforms={u_time:new this.minigl.Uniform({value:0}),u_shadow_power:new this.minigl.Uniform({value:5}),u_darken_top:new this.minigl.Uniform({value:this.el.dataset.jsDarkenTop===""?1:0}),u_active_colors:new this.minigl.Uniform({value:this.activeColors,type:"vec4"}),u_global:new this.minigl.Uniform({value:{noiseFreq:new this.minigl.Uniform({value:[this.freqX,this.freqY],type:"vec2"}),noiseSpeed:new this.minigl.Uniform({value:5e-6})},type:"struct"}),u_vertDeform:new this.minigl.Uniform({value:{incline:new this.minigl.Uniform({value:Math.sin(this.angle)/Math.cos(this.angle)}),offsetTop:new this.minigl.Uniform({value:-.5}),offsetBottom:new this.minigl.Uniform({value:-.5}),noiseFreq:new this.minigl.Uniform({value:[3,4],type:"vec2"}),noiseAmp:new this.minigl.Uniform({value:this.amp}),noiseSpeed:new this.minigl.Uniform({value:10}),noiseFlow:new this.minigl.Uniform({value:3}),noiseSeed:new this.minigl.Uniform({value:this.seed})},type:"struct",excludeFrom:"fragment"}),u_baseColor:new this.minigl.Uniform({value:this.sectionColors[0],type:"vec3",excludeFrom:"fragment"}),u_waveLayers:new this.minigl.Uniform({value:[],excludeFrom:"fragment",type:"array"})};for(let i=1;i<this.sectionColors.length;i+=1)this.uniforms.u_waveLayers.value.push(new this.minigl.Uniform({value:{color:new this.minigl.Uniform({value:this.sectionColors[i],type:"vec3"}),noiseFreq:new this.minigl.Uniform({value:[2+i/this.sectionColors.length,3+i/this.sectionColors.length],type:"vec2"}),noiseSpeed:new this.minigl.Uniform({value:11+.3*i}),noiseFlow:new this.minigl.Uniform({value:6.5+.3*i}),noiseSeed:new this.minigl.Uniform({value:this.seed+10*i}),noiseFloor:new this.minigl.Uniform({value:.1}),noiseCeil:new this.minigl.Uniform({value:.63+.07*i})},type:"struct"}));return this.vertexShader=[this.shaderFiles.noise,this.shaderFiles.blend,this.shaderFiles.vertex].join(`

`),new this.minigl.Material(this.vertexShader,this.shaderFiles.fragment,this.uniforms)}initMesh(){this.material=this.initMaterial(),this.geometry=new this.minigl.PlaneGeometry,this.mesh=new this.minigl.Mesh(this.geometry,this.material)}shouldSkipFrame(i){return!!window.document.hidden||!this.conf.playing||parseInt(i,10)%2==0||void 0}updateFrequency(i){this.freqX+=i,this.freqY+=i}toggleColor(i){this.activeColors[i]=this.activeColors[i]===0?1:0}showGradientLegend(){this.width>this.minWidth&&(this.isGradientLegendVisible=!0,document.body.classList.add("isGradientLegendVisible"))}hideGradientLegend(){this.isGradientLegendVisible=!1,document.body.classList.remove("isGradientLegendVisible")}init(){this.initGradientColors(),this.initMesh(),this.resize(),requestAnimationFrame(this.animate),window.addEventListener("resize",this.resize)}waitForCssVars(){if(this.computedCanvasStyle&&this.computedCanvasStyle.getPropertyValue("--gradient-color-1").indexOf("#")!==-1)this.init(),this.addIsLoadedClass();else{if(this.cssVarRetries+=1,this.cssVarRetries>this.maxCssVarRetries)return this.sectionColors=[16711680,16711680,16711935,65280,255],void this.init();requestAnimationFrame(()=>this.waitForCssVars())}}initGradientColors(){this.sectionColors=["--gradient-color-1","--gradient-color-2","--gradient-color-3","--gradient-color-4"].map(i=>{let r=this.computedCanvasStyle.getPropertyValue(i).trim();return r.length===4&&(r=`#${r.substr(1).split("").map(h=>h+h).join("")}`),r&&`0x${r.substr(1)}`}).filter(Boolean).map(X)}}const Z={class:"fx"},ee=D({__name:"Background",setup(d){const i=y(null);return N(()=>{var r=new Q;r.initGradient("#gradient-canvas")}),(r,v)=>(C(),A("div",Z,[_("canvas",{ref_key:"canvas",ref:i,id:"gradient-canvas"},null,512)]))}});const te=F(ee,[["__scopeId","data-v-ec0339b0"]]),ne=["src","alt"],ie={class:"detail"},se={class:"desc"},oe={class:"name"},ae=D({__name:"Achievement",setup(d){const{targetList:i,shiftTarget:r}=V(),{vLongPress:v}=W(),h=y(null),a=y(!0),p=y(!1),n=O(()=>i.value.length>=1?i.value[0]:null);L(n,()=>{a.value=!0,setTimeout(()=>{a.value=!1},1e3)}),L(i,()=>{var s,l;i.value.length>0?(s=h.value)==null||s.open():(l=h.value)==null||l.closeDialog()},{deep:!0});const w=()=>{r()};return(s,l)=>(C(),T(P,{ref_key:"popupRef",ref:h,onTouchMask:w,className:"target"},{default:R(()=>[m(n)?(C(),A("div",{key:0,class:$(["sticker-box",{change:a.value},{"img-leave":p.value}]),onClick:w},[I(_("img",{onClick:l[0]||(l[0]=q(()=>{},["stop"])),src:m(n).icon,class:"sticker",alt:m(n).name},null,8,ne),[[m(v),{module:"target",id:m(n).id}]]),_("div",ie,[_("div",se,M(m(n).desc||"你发现了一张贴纸"),1),_("div",oe,M(m(n).name),1)])],2)):E("",!0)]),_:1},512))}});const re=F(ae,[["__scopeId","data-v-5b1d0de0"]]),le=()=>{window.addEventListener("beforeunload",function(){K("leave")})};function ce(){const d=y(!1),i=y(""),r=y(0);return{initVisit:async h=>{le(),d.value=!0;const{data:a}=await H().catch(p=>({data:null}));d.value=!1,i.value=a.time||"",r.value=Date.now(),setTimeout(async()=>{const p=await Y(r.value);h&&h(p.data)},10*60*1e3+1e4)},visitLoading:d,lastVisit:i}}const de={class:"container"},ue=D({__name:"BasicLayout",setup(d){const{addTarget:i}=V(),{initVisit:r,visitLoading:v,lastVisit:h}=ce();return G(()=>{r(a=>{i(a)})}),L(h,a=>{if(a){if(new Date(h.value).getFullYear()!==2023)return;i([{id:"2024",name:"365日不见，如隔一年",comment:"365日不见，如隔一年",icon:"https://storage.expingworld.com/cms/sticker_10_9247b3dc0b.png",addTime:Date.now().toString(),desc:" "}])}},{immediate:!0}),(a,p)=>{const n=k("router-view");return C(),A(j,null,[z(m(te)),_("div",de,[m(v)?E("",!0):(C(),T(n,{key:0}))]),z(m(re))],64)}}});const me=F(ue,[["__scopeId","data-v-15f7222f"]]);export{me as default};
