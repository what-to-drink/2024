import { request } from '@/utils'

export const getPassLog = () => request('/api/game/pass_log')

export const startGame = (gameId: number | string) => request('/api/game/start', { params: { gameId } })

export const gameActionLog = (params: {
  position?: number | string
  answer?: string | number
  gameId: number | string
  logId: string
}) => request('/api/game/action', { params: params })

export const gameSave = (params: {
  action?: number | string
  answer?: string | number
  gameId: number | string
  logId: string
}) => request('/api/game/save', { params: params })

export const teaTopLevel = () => request('/api/game/tea_top_level')
