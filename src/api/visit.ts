import { request } from '@/utils'

export const report = (action: string) => {
  navigator.sendBeacon(
    import.meta.env.VITE_API_BASE_URL +
      '/api/visit/log?year=2024&token=' +
      window.localStorage.getItem('milk_vote_2023_token') +
      '&action=' +
      action
  )
}

export const getLastVisitTime = () => {
  return request('/api/visit/last')
}

export const getVisitTimeTarget = (time: number) => request('/api/visit/time_target', { params: { time } })

export const shareLog = (module: string, moduleId?: string | number) => {
  navigator.sendBeacon(
    import.meta.env.VITE_API_BASE_URL +
      '/api/visit/share?year=2024&token=' +
      window.localStorage.getItem('milk_vote_2023_token') +
      '&module=' +
      module +
      '&moduleId=' +
      moduleId
  )
}
