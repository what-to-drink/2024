import { shareLog } from '@/api/visit'
import { DirectiveBinding } from 'vue'

interface LongPressDirectiveOptions {
  id?: string | number
  module: string
  callback?: (module: string, id?: string | number) => void
  duration?: number
}

export const useLongPress = () => {
  const vLongPress = {
    beforeMount(el: HTMLElement, binding: DirectiveBinding<LongPressDirectiveOptions>) {
      let pressTimer: number | null = null

      const report = (module: string, id?: string | number) => {
        // 上报接口
        shareLog(module, id)
      }
      const start = (e: MouseEvent | TouchEvent) => {
        if (e.type === 'click' && (e as MouseEvent).button !== 0) {
          return
        }

        if (pressTimer === null) {
          pressTimer = window.setTimeout(() => {
            const _binding = binding.value
            // 触发长按后的操作，这里可以调用接口
            _binding?.callback && _binding?.callback(_binding.module, _binding.id)

            report(_binding.module, _binding.id)
          }, binding.value?.duration || 500)
        }
      }

      const cancel = () => {
        if (pressTimer !== null) {
          clearTimeout(pressTimer)
          pressTimer = null
        }
      }

      // 添加事件监听器
      el.addEventListener('mousedown', start)
      el.addEventListener('touchstart', start)

      // 取消事件监听器
      el.addEventListener('click', cancel)
      el.addEventListener('mouseout', cancel)
      el.addEventListener('touchend', cancel)
      el.addEventListener('touchcancel', cancel)
    }
  }
  return {
    vLongPress: vLongPress
  }
}
