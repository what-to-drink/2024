import { InjectionKey, Ref, inject, provide, ref } from 'vue'

// 定义全局状态的类型
interface TragetState {
  targetList: Ref<Target[]>
  addTarget: (targets: Target[]) => void
  shiftTarget: () => void
}

export type Target = {
  id: string
  name: string
  comment: string
  icon: string
  addTime: string
  desc?: string
}

// 创建一个空对象，用于存储全局状态
// const targetListSymbol: InjectionKey<Ref<Target[]>> = Symbol('targetList')
const targetList = ref<Target[]>([])

// 提供一个函数，用于在应用程序中的组件中访问全局状态
// export const provideGlobalTarget = () => {
//   provide(targetListSymbol, targetList)
// }

export const useTarget = (): TragetState => {
  // const state = inject(targetListSymbol)
  // if (!state) {
  //   throw new Error('useGlobalState must be used within a component wrapped with provideGlobalState')
  // }

  const addTarget = (target: Target[]) => {
    targetList.value.push(...target)
  }
  const shiftTarget = () => {
    targetList.value.shift()
  }

  return { targetList, addTarget, shiftTarget }
}
