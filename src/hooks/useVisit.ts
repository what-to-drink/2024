import { getLastVisitTime, getVisitTimeTarget, report } from '@/api/visit'
import { ref } from 'vue'

const addEventListener = () => {
  window.addEventListener('beforeunload', function () {
    report('leave')
  })
}

export function useVisit() {
  const visitLoading = ref(false)
  const lastVisit = ref<string>('')
  const intoTime = ref(0)

  const initVisit = async (callback?: (data: any) => void) => {
    addEventListener()
    visitLoading.value = true
    const { data: last } = await getLastVisitTime().catch((res) => ({ data: null }))
    visitLoading.value = false
    lastVisit.value = last.time || ''
    intoTime.value = Date.now()

    // 开始10分钟倒计时
    setTimeout(async () => {
      const res = await getVisitTimeTarget(intoTime.value)
      callback && callback(res.data)
    }, 10 * 60 * 1000 + 10000)
  }

  return {
    initVisit,
    visitLoading,
    lastVisit
  }
}
