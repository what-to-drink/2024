import { createApp } from 'vue'
import App from './App.vue'

import Vue3Lottie from 'vue3-lottie'
import 'vue3-lottie/dist/style.css'
import router from './router'

const app = createApp(App)
app.use(Vue3Lottie).use(router).mount('#app')
