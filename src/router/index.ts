import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import('../layout/BasicLayout.vue'),
      redirect: '/index',
      children: [
        { path: '/index', name: 'Home', component: () => import('../modules/index/App.vue') },
        { path: '/game/:id', name: 'Game', component: () => import('../modules/game/App.vue') },
        {
          path: '/wish',
          name: 'Wish',
          component: () => import('../modules/wish/App.vue')
        }
      ]
    },
    {
      path: '/:pathMatch(.*)',
      name: 'notfound',
      redirect: '/index'
    }
  ]
})

export default router
